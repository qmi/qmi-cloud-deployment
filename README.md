# QMI CLOUD DEPLOYMENT

This repository contains the Docker Stack deployment for QMI Cloud

# Prerequisties

- Docker installed
- Docker stack NODE initiated

## Deployment steps

### 1. Edit environment variables file 'env.txt'

``` 
IMAGE_TAG="1.1.3"
HOSTNAME_URL="http://localhost:3000"
GIT_SCENARIOS="git::git@gitlab.com:qmi/qmi-cloud-scenarios.git"
GIT_TAG="master"

LOCAL_MONGO=0
MONGO_URI="mongodb://root:example@192.168.1.44:27017/qmicloud?authSource=admin"
CERT_PFX_FILENAME="name of the PFX file stored in folder 'certs'"
CERT_PFX_PASSWORD='password for the PFX file'
```

### 2. Create 'secret.json' file with correspoding information
```json
{
    "AZURE_TENANT_ID" : "XXX",
    "AZURE_CLIENT_ID": "YYY",
    "AZURE_CLIENT_SECRET": "ZZZ"
}
```

### 3. Run 

```shell
    
./stack-deploy.sh

```