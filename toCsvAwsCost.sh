#!/bin/bash
echo "---------"
BASEDIR=/home/QMI/aws-s3-qmi-reports
thisMonth=`date +%m`
nextMonth=`date -d "-$(date +%d) days +2 month" +%m`
folder="$BASEDIR/monthly/monthly/2022$(echo $thisMonth)01-2022$(echo $nextMonth)01"
JSONS=`find $folder -maxdepth 1 -type f -name "*.json"`

date

echo $JSONS

for i in $JSONS; do  
    f=`jq -r '.reportKeys[0]' $i`
    name=`jq -r '.billingPeriod.start' $i`
    name2=`echo $name | cut -c1-6`
    echo "Extracting file: $BASEDIR/$f"
    echo "Into here: /home/QMI/tempReports/$name2.csv"
    gunzip < $BASEDIR/$f > /home/QMI/tempReports/$name2.csv
    mkdir -p $BASEDIR/tempReports
    cp /home/QMI/tempReports/$name2.csv $BASEDIR/tempReports/$name2.csv 
    echo "Copied /home/QMI/tempReports/$name2.csv INTO $BASEDIR/tempReports/$name2.csv"
done


