#!/bin/bash

echo "--- Executing: $0 $@"
now=`date`
echo "--- Date: $now"

az login --identity > /dev/null 2>&1

#Provide the subscription Id where snapshot is created
subscriptionId="62ebff8f-c40b-41be-9239-252d6c0c8ad9"

#Provide the name of your resource group where snapshot is created
targetRg=$2

#Provide the snapshot name 

rg=$1
az account set --subscription $subscriptionId

az disk list --resource-group $rg --query "[].{id:id,name:name}"  |  jq -c '.[]'  | while read i; do
    diskId=$(echo "$i" | jq -r '.id')
    snapName=Snap_$(echo "$i" | jq -r '.name')
    snapName2=${snapName::-5}_$(date +'%Y-%m-%d_%H-%M')
    echo "$diskId ---> $snapName2"

    az snapshot create -g $targetRg -n $snapName2 --source $diskId --location eastus

done


