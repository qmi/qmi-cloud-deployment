#Provide the subscription Id where snapshot is created
subscriptionId="62ebff8f-c40b-41be-9239-252d6c0c8ad9"

#Provide the name of your resource group where snapshot is created
resourceGroupName="QMI-Machines"

#Provide the snapshot name 
snapshotName=$1

#Provide Shared Access Signature (SAS) expiry duration in seconds e.g. 3600.
#Know more about SAS here: https://docs.microsoft.com/en-us/azure/storage/storage-dotnet-shared-access-signature-part-1
sasExpiryDuration=43200

#Provide storage account name where you want to copy the snapshot. 
storageAccountNameSEA="machinesnapshotssea"

#Name of the storage container where the downloaded snapshot will be stored
storageContainerName=snapshots

#Provide the key of the storage account where you want to copy snapshot. 
storageAccountKeySEA="yYSNAQrhYRWYQTBXW/gYHXX0EoeYP5CPAbXhmlF6x6zHIcfzyGNtFwz2lAOh/89ShgHCzdocjtE7yom8FabiPg=="


#Provide the name of the VHD file to which snapshot will be copied.
destinationVHDFileName=$snapshotName

az account set --subscription $subscriptionId

sas=$(az snapshot grant-access --resource-group $resourceGroupName --name $snapshotName --duration-in-seconds $sasExpiryDuration --query [accessSas] -o tsv)


az storage blob copy start --destination-blob $destinationVHDFileName --destination-container $storageContainerName --account-name $storageAccountNameSEA --account-key $storageAccountKeySEA --source-uri $sas
