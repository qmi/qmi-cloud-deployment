#!/bin/bash

set +H
# Load enviroment variables
set -o allexport
[[ -f env.txt ]] && source env.txt
set +o allexport


echo "---- ENVS ----"
echo "IMAGE_TAG_APP=$IMAGE_TAG_APP"
echo "IMAGE_TAG_WORKER=$IMAGE_TAG_WORKER"
echo "IMAGE_TAG_CLI=$IMAGE_TAG_CLI"
echo "IMAGE_TAG_CLI=$IMAGE_TAG_WEBHOOK"
echo "HOSTNAME_URL=$HOSTNAME_URL"
echo "MONGO_URI=$MONGO_URI"
echo "LOCAL_MONGO=$LOCAL_MONGO"
echo "CERT_PFX_FILENAME=$CERT_PFX_FILENAME"
echo "CERT_PFX_PASSWORD=$CERT_PFX_PASSWORD"
echo "GIT_SCENARIOS=$GIT_SCENARIOS"
echo "GIT_TAG=$GIT_TAG"
echo "AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID"
echo "AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY"
echo "--------------"

docker pull qlikgear/qmi-cloud-webhook:$1
docker service update --image qlikgear/qmi-cloud-webhook:$1 qmicloud_qmihook_start
docker service update --image qlikgear/qmi-cloud-webhook:$1 qmicloud_qmihook_stop

