#!/bin/bash

set +H
# Load enviroment variables
set -o allexport
[[ -f env.txt ]] && source env.txt
set +o allexport

echo "---- ENVS ----"
echo "IMAGE_TAG_APP=$IMAGE_TAG_APP"
echo "IMAGE_TAG_WORKER=$IMAGE_TAG_WORKER"
echo "IMAGE_TAG_CLI=$IMAGE_TAG_CLI"
echo "IMAGE_TAG_WEBHOOK=$IMAGE_TAG_WEBHOOK"
echo "HOSTNAME_URL=$HOSTNAME_URL"
echo "MONGO_URI=$MONGO_URI"
echo "LOCAL_MONGO=$LOCAL_MONGO"
echo "CERT_PFX_FILENAME=$CERT_PFX_FILENAME"
echo "CERT_PFX_PASSWORD=$CERT_PFX_PASSWORD"
echo "GIT_SCENARIOS=$GIT_SCENARIOS"
echo "GIT_TAG=$GIT_TAG"
echo "AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID"
echo "AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY"
echo "--------------"

echo "-- Pulling images"
docker pull $DOCKERIMAGE_TERRAFORM
docker pull mcr.microsoft.com/azure-powershell:4.2.0-ubuntu-18.04
docker pull qlikgear/qmi-cloud-cli:$IMAGE_TAG_CLI
docker pull qlikgear/qmi-cloud-app:$IMAGE_TAG_APP
docker pull qlikgear/qmi-cloud-worker:$IMAGE_TAG_WORKER
docker pull qlikgear/qmi-cloud-webhook:$IMAGE_TAG_WEBHOOK

if [ $LOCAL_MONGO -eq 1 ]; then
   echo "-- Deploying stack LOCAL_MONGO"
   docker stack deploy --compose-file ./stack/stack-dev-mongo.yaml qmimongo 
   sleep 10
fi

echo "-- Deploying stack QMICLOUD"
docker stack deploy --compose-file ./stack/stack.yaml qmicloud 


sleep 60
docker container prune -f

