#!/bin/bash

docker pull qlikgear/qmi-cloud-worker:$1
docker service update --image qlikgear/qmi-cloud-worker:$1 qmicloud_worker
